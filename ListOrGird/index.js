/**
 * Created by An on 2016/4/13.
 */
$(document).ready(function () {


    for (var i = 0; i < 5; i++) {

        var ulTag = $("<ul>").addClass("uls").appendTo($(".body"));
        var liTag = $("<li>").addClass("lis").appendTo(ulTag);
        var divTag = $("<div>").addClass("box").appendTo(liTag);
        $("<img>").attr("src", "bg.png").appendTo(divTag);
        var divPT = $("<div>").addClass("pContent").appendTo(divTag);
        $("<p>").addClass("p1").text("Android的应用与开发").appendTo(divPT);
        $("<p>").addClass("p11").text("Android是一种基于Linux的自由及开放源代码的操作系统，主要使用于移动设备，如智能手机和平板电脑，由Google公司和开放手机联盟领导及开发").appendTo(divPT);
        $("<p>").addClass("p2").text("4课时　30分钟").appendTo(divPT);
        $("<span>").appendTo(divTag);
    }


    $(".title .left li").hover(function () {
        var liW = $(".title .left li a").outerWidth();
        console.log(liW)
        $(this).css({backgroundColor: "#fff"});
        $(this).find(".down").css({display: "block", backgroundColor: "#fff"});

    }, function () {
        $(this).find(".down").css({display: "none"});
        $(this).css({backgroundColor: "#f5f5f5"});
        $(".title .left li:nth-child(1)").css({backgroundColor: "#fff"});
    });

    $(".p11").css({display: "none"});

    $(".lst1").on("click", function () {
        $(this).css({backgroundPosition: "0px -26px"});
        $(".lst2").css({backgroundPosition: "-30px -26px"});
        setCss(".uls", "lis-v", "lis");
        setCss(".lis", "box-v", "box");
        $(".p11").css({display: "none"});


    });
    $(".lst2").on("click", function () {
        $(this).css({backgroundPosition: "-30px 0px"});
        $(".lst1").css({backgroundPosition: "0px 0px"});
        setCss(".uls", "lis", "lis-v");
        setCss(".lis-v", "box", "box-v");
        $(".p11").css({display: "block"});
    });

    //$(".uls .lis").hover(function () {
    //
    //    $(this).find(".p11").css({display: "block"});
    //    $(this).css({zIndex: 6}).siblings().css({zIndex: 1})
    //
    //}, function () {
    //    $(".p11").css({display: "none"});
    //});


});


function setCss(_id1, _id2, _id3) {
    $(_id1).children().removeClass(_id2).addClass(_id3);
}
