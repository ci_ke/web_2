/**
 * Created by An on 2016/4/6.
 */

//自定义数据源 json格式


$(document).ready(function () {

    var imgUrl =
    {
        "url": [
            {"src": "1.jpg"},
            {"src": "2.jpg"},
            {"src": "3.jpg"},
            {"src": "4.jpg"},
            {"src": "5.jpg"},
            {"src": "6.jpg"},
        ]
    };

    var len = imgUrl.url.length;

    for (var j = 0; j < len; j++) {

        //var li = $("<li>").appendTo($(".img"));
        //var imgLi = $("<a>").attr("href", "#").appendTo(li);
        //$("<img>").attr("src", "images/" + imgUrl.url[j].src).appendTo(imgLi);

        var imgLi = "<li><a href='#'><img src=" + "../images/" + imgUrl.url[j].src + "></a></li>";
        $(".img").append(imgLi);

        console.log(imgUrl.url[j].src);

        var numLi = "<li>" + (j + 1) + "</li>";
        $(".u-num").append(numLi);
    }

    $(".img li").eq(0).show();
    $(".u-num li").first().addClass("sel");

    $(".u-num li").mousemove(function () {
        $(this).addClass("sel").siblings().removeClass("sel");
        var index = $(this).index();
        i = index;
        $(".img li").eq(index).stop().fadeIn(300).siblings().stop().fadeOut(300);

    });

    var i = 0;
    var t = setInterval(move, 1500);

    function move() {
        i++;
        if (i == len) {
            i = 0;
        }
        $(".u-num li").eq(i).addClass("sel").siblings().removeClass("sel");
        $(".img li").eq(i).fadeIn(300).siblings().fadeOut(300);
    }

    function moveL() {
        i--;
        if (i == -1) {
            i = len - 1;
        }
        $(".u-num li").eq(i).addClass("sel").siblings().removeClass("sel");
        $(".img li").eq(i).fadeIn(300).siblings().fadeOut(300);
    }

    $(".u-img").hover(function () {
        clearInterval(t);
    }, function () {
        t = setInterval(move, 1500);
    });
    $(".btn").hover(function () {
        $(this).css({"backgroundColor": "rgba(105,105,105,1)"});
    }, function () {
        $(this).css({"backgroundColor": "rgba(105,105,105,0.5)"});
    });

    $(".left").click(function () {
        moveL();
    });
    $(".right").click(function () {
        move();
    });


});
