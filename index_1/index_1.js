/**
 * Created by An on 2016/4/6.
 */
$(document).ready(function () {

    var imgUrL = {
        "url": [
            {"src": "1.jpg"},
            {"src": "2.jpg"},
            {"src": "3.jpg"},
            {"src": "4.jpg"}
        ]
    }

    /**
     * 初始化
     * */
    $.each(imgUrL.url, function (index, value) {

        var imgLi = "<li><a><img src=" + "../images/" + value.src + "></a></li>"
        $(".img").append(imgLi);

        var numLi = "<li></li>"
        $(".point").append(numLi);
    });

    var cloneImg = $(".img li").first().clone();
    $(".img").append(cloneImg);


    $(".point li").first().addClass("sel");


    var t = setInterval(moveRight, 1000);

    $(".img").hover(function () {
        clearInterval(t);
    }, function () {
        t = setInterval(moveRight, 1000)
    });

    $(".btn").hover(function () {
        clearInterval(t);
        $(this).css({"backgroundColor": "rgba(105, 105, 105, 1)"});
    }, function () {
        $(this).css({"backgroundColor": "rgba(105, 105, 105, 0.5)"});
        t = setInterval(moveRight, 1000);

    });

    var ii = 0;

    $(".point li").hover(function () {

        clearInterval(t);
        var index = $(this).index();
        ii = index;
        $(".img").stop().animate({"left": -730 * index}, 500);
        $(this).addClass("sel").siblings().removeClass("sel");

    }, function () {

        t = setInterval(moveRight, 1000);
    });

    var len = $(".img li").size();

    $(".left").click(moveLeft);
    $(".right").click(moveRight);

    function move(isRun) {

        if (isRun) {
            ii--;
        } else {
            i++;
        }


        if (ii == -1) {
            $(".img").css({"left": -((len - 1) * 730  )});
            ii = len - 2;
        }

        if (ii == len) {
            $(".img").css({"left": 0});
            ii = 1;
        }

        $(".img").stop().animate({"left": -730 * ii}, 500);
        if (ii == len - 1) {
            $(".point li").eq(0).addClass("sel").siblings().removeClass("sel");
        } else {

            $(".point li").eq(ii).addClass("sel").siblings().removeClass("sel");
        }
    }

    function moveLeft() {

        ii--;
        if (ii == -1) {
            $(".img").css({"left": -((len - 1) * 730  )});
            ii = len - 2;
        }

        $(".img").stop().animate({"left": -730 * ii}, 500);
        $(".point li").eq(ii).addClass("sel").siblings().removeClass("sel");
    }

    function moveRight() {

        ii++;
        if (ii == len) {
            $(".img").css({"left": 0});
            ii = 1;
        }
        $(".img").stop().animate({"left": -730 * ii}, 500);
        if (ii == len - 1) {
            $(".point li").eq(0).addClass("sel").siblings().removeClass("sel");
        } else {

            $(".point li").eq(ii).addClass("sel").siblings().removeClass("sel");
        }
    }

})
;
